import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { Editors } from '../imports/api/editors.js';

Meteor.startup(function() {
  Meteor.methods({
    compileAndRun: function(file) {
      var Future = Meteor.npmRequire('fibers/future');
      var exec = Meteor.npmRequire('child_process').exec;
      var fs    = Meteor.npmRequire('fs');

      this.unblock();

      var editorName = "temporary_name";
      var cFileName  = "/tmp/" + editorName + ".c";
      var executable = "/tmp/" + editorName;
      var gccCommand = "gcc " + cFileName + " -o " + executable;

      fs.writeFileSync(cFileName, file);

      var future = new Future();

      exec(gccCommand, function(gccErr, gccStdout, gccStderr) {
        if (gccErr) {
          future.return(gccStdout.toString() + gccStderr.toString());
        } else {
          exec(executable, function(exeErr, exeStdout, exeStderr) {
            future.return(exeStdout.toString() + exeStderr.toString());
          });
        }
      });

      Editors.editorPublish(editorName, future.wait());

      return '';
    },
  });
});

