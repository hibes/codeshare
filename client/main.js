import { Meteor } from 'meteor/meteor';
import { ReactiveVar } from 'meteor/reactive-var';
import { Editors } from '../imports/api/editors.js';

import './main.html';

Template.compileAndRun.helpers({
  compileAndRunResults() {
    return Editors.findOne({ 'name': "temporary_name" }, { fields: { 'result': 1} } ).result;
  },
});

Template.compileAndRun.events = {
  'click input' : function() {
    Meteor.call('compileAndRun', $('#editor').val(), function(err, response) {
      if (typeof err !== "undefined") {
        Session.set('serverCompileAndRunResponse', err + response);
      } else {
        Session.set('serverCompileAndRunResponse', response);
      }

      Meteor.flush();
    });
  }
};

$(document).ready(function() {
  $('#editor').attr('rows', 25);
});
