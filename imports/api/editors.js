import { Mongo } from 'meteor/mongo';

export const Editors = new Mongo.Collection('editors');

Editors.editorPublish = function (editorName, output) {
  Editors.upsert(
      { 'name': editorName },
      { 'name': editorName, 'result': output, 'time': Date.now() }
    );
}

